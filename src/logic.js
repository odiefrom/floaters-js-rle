var Logic = new Object();
Logic.inputMode = "default";
Logic.actionQueue = [];
Logic.textQueue = [];

Logic.inputProcessor = function (inputEvent) {
    if(Logic.inputMode == "default") {
        if(inputEvent.type == "keydown") {
            switch(inputEvent.key) {
                case "ArrowDown":
                case "s":
                    Logic.actionQueue.push("player_move_down");
                    break;
                case "ArrowUp":
                case "w":
                    Logic.actionQueue.push("player_move_up");
                    break;
                case "ArrowLeft":
                case "a":
                    Logic.actionQueue.push("player_move_left");
                    break;
                case "ArrowRight":
                case "d":
                    Logic.actionQueue.push("player_move_right");
                    break;
                case "e":
                    Logic.actionQueue.push("player_interact");
                    break;
            }
        }

    }
    else if(Logic.inputMode == "terminal") {
        if((inputEvent.keyCode >= 48 && inputEvent.keyCode <= 90) || (inputEvent.keyCode >= 186 && inputEvent.keyCode <= 222)) {
            Logic.textQueue.push(inputEvent.key);
        } else if(inputEvent.keyCode == 8) {
            Logic.textQueue.push("backspace");
        } else if(inputEvent.keyCode == 27) {
            Logic.textQueue.push("escape");
        } else if(inputEvent.keyCode == 32) {
            Logic.textQueue.push(" ");
        } else if(inputEvent.keyCode == 13) {
            Logic.textQueue.push("return");
        }
    }
}

Logic.processActionQueue = function () {
    // Due to JS's async nature, RLEngine.inputProcessor can append actionQueue elements while processing actionQueue.
    // As such, we duplicate the array and immediately wipe the true actionQueue, then operate on our clone data
    var tempQueue = Logic.actionQueue.slice();
    Logic.actionQueue = [];

    // Actions are "blind fired" from this function. This function doesn't check to see if actions succeed and instead passes responsibility to the appropriate entity

    // For example, if the input for "player_move_left" is pressed, this function will call RLEngine.player.moveLeft() and let the Player object check for collisions etc
    for(var i = 0; i < tempQueue.length; i++) {
        var action = tempQueue[i];
        if(action == "player_move_down") {
            RLEngine.player.moveDown();
        } else if(action == "player_move_up") {
            RLEngine.player.moveUp();
        } else if(action == "player_move_left") {
            RLEngine.player.moveLeft();
        } else if(action == "player_move_right") {
            RLEngine.player.moveRight();
        } else if(action == "player_interact") {
            RLEngine.player.interact();
        }
    }
}

Logic.processTextQueue = function() {
    var tempQueue = Logic.textQueue.slice();
    Logic.textQueue = [];

    for(var i = 0; i < tempQueue.length; i++) {
        switch (tempQueue[i]) {
            case "backspace":
                if(RLEngine.textBuffer[RLEngine.textBuffer.length-1] != "newline") {
                    RLEngine.textBuffer.pop();
                }
                break;
            case "escape":
                RLEngine.entityList.splice(RLEngine.currentScreen,1);
                RLEngine.textBuffer = []; // Temporary effect, closing a terminal SHOULD remember what it's contents were, but for right now, a flush is needed
                RLEngine.currentScreen = null;
                Logic.inputMode = "default";
                break;
            case "return":
                RLEngine.textBuffer.push("newline");
                var command = [];
                var characterIndex = RLEngine.textBuffer.length - 2;
                while(true) {
                    if(RLEngine.textBuffer[characterIndex] != "newline" && RLEngine.textBuffer[characterIndex] != " ") {
                        command.unshift(RLEngine.textBuffer[characterIndex--]);
                    } else {
                        RLEngine.entityList[RLEngine.currentScreen].processCommand(command.join(''));
                        break;
                    }
                    if(characterIndex < 0) {
                        RLEngine.entityList[RLEngine.currentScreen].processCommand(command.join(''));
                        break;
                    }
                }
                break;
            default:
                RLEngine.textBuffer.push(tempQueue[i]);
        }
    }
}

Logic.advanceRoom = function (doorTile) { // The door tile we left from
    // Load new room into gameMap
    if(doorTile.linkedRoom == "playerShip") {
        RLEngine.gameMap = RLEngine.playerShip;
    }
    else {
        RLEngine.gameMap = ShipGenerator.padRoomMap(RLEngine.foreignShip.rooms[doorTile.linkedRoom]);
    }

    // Figure out where entry door is (and open it if needed)
    var temp_x = 0;
    var temp_y = 0;

    for(var x = 0; x < 16; x++) {
        for(var y = 0; y < 16; y++) {
            if(RLEngine.gameMap[x][y].linkedRoom == doorTile.currentRoom) {
                temp_x = x;
                temp_y = y;
            }
        }
    }

    if(!RLEngine.gameMap[temp_x][temp_y].openState) RLEngine.gameMap[temp_x][temp_y].onInteract();
    RLEngine.player.x_pos = temp_x;
    RLEngine.player.y_pos = temp_y;
}