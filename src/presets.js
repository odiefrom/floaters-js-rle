var Presets = new Object();
Presets.tiles = new Object();
Presets.entities = new Object();

Presets.tiles.metalFloor = function() {
    return new Tile("metalFloor",RLEngine.res.assets.metalFloorTile, true, function(){}, function(){});
}

Presets.tiles.space = function() {
    return new Tile("space",RLEngine.res.assets.spaceTile, false, function(){}, function(){});
}

Presets.tiles.transparent = function() {
    return new Tile("transparent",RLEngine.res.defaults.transparent, false, function(){});
}

Presets.tiles.terminal = function() {
    var temp = new Tile("terminal",RLEngine.res.defaults.terminal, false, function(){}, function(){});

    temp.onInteract = function() {
        RLEngine.currentScreen = RLEngine.entityList.push(Presets.entities.terminalScreen()) - 1;
        Logic.inputMode = "terminal";
    }

    return temp;
}

Presets.tiles.door = function() {
    var temp = new Tile("door",RLEngine.res.defaults.doorClosed,false,function(){},function(){});
    temp.openState = false;
    temp.linkedRoom = "";
    temp.currentRoom = "";
    temp.doorName = "";
    temp.onInteract = function() {
        if(this.openState) {
            this.img = RLEngine.res.defaults.doorClosed;
            this.walkableFlag = false;
            this.openState = false;
        } else {
            this.img = RLEngine.res.defaults.doorOpen;
            this.walkableFlag = true;
            this.openState = true;
        }
    }
    temp.onCollision = function() {
        if(this.openState) {
            Logic.advanceRoom(this); // destination label, origin label
        }
    }
    return temp;
}

Presets.entities.terminalScreen = function() {
    var temp = new Entity("terminalScreen",1,1,700,700,RLEngine.res.assets.terminalScreen,null,null,null,null,function(){});

    temp.processCommand = function(command, args) {
        var resultStringArray = [];
        switch(command) {
            case "help":
            case "?":
                resultStringArray = resultStringArray.concat("=========================================".split('')); resultStringArray.push("newline");
                resultStringArray = resultStringArray.concat("help/?: This screen.".split('')); resultStringArray.push("newline");
                resultStringArray = resultStringArray.concat("info: System Information.".split('')); resultStringArray.push("newline");
                resultStringArray = resultStringArray.concat("new_block_ship: Generate a new block type ship.".split('')); resultStringArray.push("newline");
                resultStringArray = resultStringArray.concat("exit: Close terminal.".split('')); resultStringArray.push("newline");
                resultStringArray = resultStringArray.concat("=========================================".split('')); resultStringArray.push("newline");
                break;
            case "info":
                resultStringArray = resultStringArray.concat("=========================================".split('')); resultStringArray.push("newline");
                resultStringArray = resultStringArray.concat("CWI Systems Terminal v.1.27".split('')); resultStringArray.push("newline");
                resultStringArray = resultStringArray.concat("128,000Gb MEM OK".split('')); resultStringArray.push("newline");
                resultStringArray = resultStringArray.concat("CPU Check... 1 2 3 4 5 6 OK".split('')); resultStringArray.push("newline");
                resultStringArray = resultStringArray.concat("Unauthorized access is prohibited.".split('')) ;resultStringArray.push("newline");
                resultStringArray = resultStringArray.concat("=========================================".split('')); resultStringArray.push("newline");
                break;
            case "new_block_ship":
                resultStringArray = resultStringArray.concat("=========================================".split('')); resultStringArray.push("newline");
                resultStringArray = resultStringArray.concat("Generating new block ship...".split('')); resultStringArray.push("newline");
                resultStringArray = resultStringArray.concat("New block ship should be outside the door".split('')); resultStringArray.push("newline");
                resultStringArray = resultStringArray.concat("=========================================".split('')); resultStringArray.push("newline");
                ShipGenerator.BlockTypeGenerator();
                break;
            case "exit":
                RLEngine.textBuffer = [];
                Logic.inputMode = "default";
                RLEngine.entityList.splice(RLEngine.currentScreen,1);
                RLEngine.currentScreen = null;
                break;
            default:
        }
        RLEngine.textBuffer = RLEngine.textBuffer.concat(resultStringArray);
    }

    return temp;
}