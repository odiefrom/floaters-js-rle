// Player is a special entity with more properties due to having more stats, movable, etc.
// Player has the base entity implementation to ensure compatibility

function Player
( // Constructor param block
    x_pos,
    y_pos,
    width,
    height,
    img_default,
    img_up,
    img_down,
    img_left,
    img_right,
    onInteract
)

{ // Constructor function
    this.x_pos = x_pos;
    this.y_pos = y_pos;
    this.width = width;
    this.height = height;
    
    this.img = new Object();
    this.img.default = img_default;
    this.img.up = img_up;
    this.img.down = img_down;
    this.img.left = img_left;
    this.img.right = img_right;

    this.direction = "down";

    //this.x_width = img.clientWidth;
    //this.y_height = img.clientHeight;

    this.onInteract = onInteract;

    this.getImg = function() {
        return this.img[this.direction];
    }

    this.moveLeft = function() {
        if(this.direction != "left") {
            this.direction = "left";
            return;
        }
        if(RLEngine.gameMap[this.x_pos-1][this.y_pos].walkableFlag) {
            this.x_pos -= 1;
            RLEngine.gameMap[this.x_pos][this.y_pos].onCollision();
        }
    }

    this.moveRight = function() {
        if(this.direction != "right") {
            this.direction = "right";
            return;
        }
        if(RLEngine.gameMap[this.x_pos+1][this.y_pos].walkableFlag) {
            this.x_pos += 1;
            RLEngine.gameMap[this.x_pos][this.y_pos].onCollision();
        }
    }

    this.moveUp = function() {
        if(this.direction != "up") {
            this.direction = "up";
            return;
        }
        if(RLEngine.gameMap[this.x_pos][this.y_pos-1].walkableFlag) {
            this.y_pos -= 1;
            RLEngine.gameMap[this.x_pos][this.y_pos].onCollision();
        }
    }

    this.moveDown = function() {
        if(this.direction != "down") {
            this.direction = "down";
            return;
        }
        if(RLEngine.gameMap[this.x_pos][this.y_pos+1].walkableFlag) {
            this.y_pos += 1;
            RLEngine.gameMap[this.x_pos][this.y_pos].onCollision();
        }
    }

    this.interact = function() {
        var interactLocation_x = this.x_pos;
        if(this.direction == "left") interactLocation_x -= 1;
        else if(this.direction == "right") interactLocation_x += 1;

        var interactLocation_y = this.y_pos;
        if(this.direction == "up") interactLocation_y -= 1;
        else if(this.direction == "down") interactLocation_y += 1;

        console.log("This is what I see in front of me: " + RLEngine.gameMap[interactLocation_x][interactLocation_y].name);

        RLEngine.gameMap[interactLocation_x][interactLocation_y].onInteract();
    }
}