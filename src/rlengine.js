var RLEngine = new Object();

RLEngine.width = 800;
RLEngine.height = 800;
RLEngine.entityList = [];
RLEngine.gameMap = [];
RLEngine.textBuffer = [];
RLEngine.currentScreen = null; // ID to current terminal screen that exists in entityList
RLEngine.player = null; // Duplicate reference to player that exists in entityList as well
RLEngine.playerShip = null; // This is just a room, not an entire ship object
RLEngine.foreignShip = null;
RLEngine.res = null;

RLEngine.start = function () {
    RLEngine.loadAssets();
    RenderEngine.loadCanvas();
    document.addEventListener("keydown",Logic.inputProcessor);
}

RLEngine.loadAssets = function () {
    console.log("RLEngine: Loading resources...");
    
    /*
    This block goes through all the script tags on the document
    and determines which are in the 'res' folder.
    
    Then, it takes the subfolder name, and uses that to create an
    entry in the resList array. This array is what is used to access
    the JSONP scripts in assetLoader.js
    
    Now, if a new resource folder is included, then it only needs to
    be added to index.html. The rest of the engine should find the
    files on its own.
    */
    let resList = [];
    for(let i = 0; i < document.scripts.length; i++) {
        let srcTag;
        try{
           srcTag = document.scripts.item(i).attributes["src"].value;
           if(srcTag.startsWith("res")){
               resList.push(srcTag.split('/')[1]);
           }
        }
        catch(error) {
            
        }
    }
    
    AssetLoader.loadResources(resList, function() {
        console.log("RLEngine: Resources loaded. Continuing load.");
        RLEngine.launchMenu();
    });
}

RLEngine.prepareGameMap = function () {
    for(var x = 0; x < 16; x++) {
        RLEngine.gameMap[x] = [];
        for(var y = 0; y < 16; y++) {
            RLEngine.gameMap[x][y] = Presets.tiles.transparent();
        }
    }
}

RLEngine.launchMenu = function () {
    RenderEngine.background = RLEngine.res.assets.spaceBackground;
    
    RLEngine.prepareGameMap();

    RLEngine.entityList.push(new Entity("titleheader",0,7,800,100,RLEngine.res.gui.titleheader,null,null,null,null,function() {}));

    setTimeout(RLEngine.gameTick, 33);
}

RLEngine.gameTick = function () {
    // Logic...
    Logic.processActionQueue();
    Logic.processTextQueue();

    // Draw Background
    RenderEngine.drawBackground();

    // Draw Tiles
    RenderEngine.drawTiles(RLEngine.gameMap);

    // Draw Entities
    RenderEngine.drawEntities(RLEngine.entityList);

    // Draw Text
    RenderEngine.drawText(RLEngine.textBuffer);

    setTimeout(RLEngine.gameTick, 33);
}

RLEngine.canvasClickHandler = function (clickEvent) {

    RLEngine.entityList = [];

    RLEngine.playerShip = ShipGenerator.playerShip();
    RLEngine.playerShip = ShipGenerator.padRoomMap(RLEngine.playerShip.entryRoom);
    RLEngine.gameMap = RLEngine.playerShip;

    var tempPlayer = new Player(6,6,1,1,RLEngine.res.defaults.icon,RLEngine.res.defaults.playerUp,RLEngine.res.defaults.playerDown,RLEngine.res.defaults.playerLeft,RLEngine.res.defaults.playerRight,function(){});
    RLEngine.entityList.push(tempPlayer);
    RLEngine.player = tempPlayer;

    RLEngine.entityList.forEach(entity => {
        if(
            clickEvent.offsetX >= entity.x_pos && 
            clickEvent.offsetY >= entity.y_pos && 
            clickEvent.offsetX <= entity.x_pos+entity.width && 
            clickEvent.offsetY <= entity.y_pos+entity.height
        ) { entity.onclick(); }
    });

    RenderEngine.canvasElement.onclick = function() {};
}