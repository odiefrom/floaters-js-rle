var AssetLoader = new Object();

AssetLoader.lists = new Object();
AssetLoader.totals = new Object();
AssetLoader.readyTotals = new Object();
AssetLoader.loadedFlags = new Object();
AssetLoader.allLoadedFlag = false;

AssetLoader.loadResources = function (categories, engineResponse) {
    console.log("AssetLoader: Start loading resources");
    RLEngine.res = new Object(); // Instantiate/clear resource object

    categories.forEach(function(category) {
        AssetLoader.loadCategory(category);
    });

    setTimeout(function() { AssetLoader.monitorLoad(categories, engineResponse) }, 300);
}

AssetLoader.loadCategory = function (resourceCategory) {
    console.log("AssetLoader: Start loading resource type: " + resourceCategory);
    RLEngine.res[resourceCategory] = new Object();
    AssetLoader.loadedFlags[resourceCategory] = false;
	
	// NEW LOADING OF JSONP FILES
	// JSON from the JSONP file must end up in AssetLoader.lists[resourceCategory]
	AssetLoader.lists[resourceCategory] = window[resourceCategory].getJSON();
	AssetLoader.processList(resourceCategory);
}

AssetLoader.processList = function (resourceCategory) {
    AssetLoader.totals[resourceCategory] = AssetLoader.lists[resourceCategory].files.length;
    AssetLoader.readyTotals[resourceCategory] = 0;
    AssetLoader.lists[resourceCategory].files.forEach(function(element) {
        RLEngine.res[resourceCategory][element[0]] = new Image();
        RLEngine.res[resourceCategory][element[0]].onloadend = function() {
            AssetLoader.readyTotals[resourceCategory]++;
        };
        RLEngine.res[resourceCategory][element[0]].src = "./res/" + resourceCategory + "/" + element[1];
    });
}

AssetLoader.monitorLoad = function (categories, callback) {
    for(var i = 0; i < categories.length; i++) {
        if(AssetLoader.totals[categories[i]] == AssetLoader.readyTotals[categories[i]]) {
            console.log("AssetLoader: All " + categories[i] + " resources loaded!");
            AssetLoader.loadedFlags[categories[i]] = true;
        }
    }
    var tempFlag = true;
    for(var i = 0; i < AssetLoader.loadedFlags; i++) {
        if(AssetLoader.loadedFlags[i] != true) {
            tempFlag = false;
            break;
        }
    }
    if(tempFlag) {
        AssetLoader.allLoadedFlag = true;
        callback();
    } else {
        setTimeout(function() { AssetLoader.monitorLoad(categories, engineResponse) }, 300);
    }
}