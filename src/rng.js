RNGModule = new Object();

RNGModule.modulus = Math.pow(2,32);
RNGModule.multiplier = 1664525;
RNGModule.increment = 1013904223;

RNGModule.seed = Date.now().toString();

RNGModule.nextResult = function() {
    RNGModule.seed = (RNGModule.multiplier * RNGModule.seed + RNGModule.increment) % RNGModule.modulus;
    return RNGModule.seed >> 16;
}