function Tile
(
    name,
    img,
    walkableFlag,
    onInteract,
    onCollision
)
{
    this.name = name;
    this.img = img;
    this.walkableFlag = walkableFlag;
    this.onInteract = onInteract;
    this.onCollision = onCollision;
}