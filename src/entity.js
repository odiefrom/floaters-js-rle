function Entity
( // Constructor param block
    name,
    x_pos,
    y_pos,
    width,
    height,
    img_default,
    img_up,
    img_down,
    img_left,
    img_right,
    onInteract
)

{ // Constructor function
    this.name = name;
    this.x_pos = x_pos;
    this.y_pos = y_pos;
    this.width = width;
    this.height = height;

    this.img = new Object();
    this.img.default = img_default;
    this.img.up = img_up;
    this.img.down = img_down;
    this.img.left = img_left;
    this.img.right = img_right;

    this.direction = "default";

    //this.x_width = img.clientWidth;
    //this.y_height = img.clientHeight;

    this.onInteract = onInteract;

    this.getImg = function() {
        return this.img[this.direction];
    }
}