var RenderEngine = new Object();

RenderEngine.canvasElement = null;
RenderEngine.context = null;
RenderEngine.background = null;

RenderEngine.text = new Object();
RenderEngine.text.fillStyle = "green";
RenderEngine.text.font = "24px Courier New";
RenderEngine.text.textToDraw = "1234567890123456789012345678901234567890";
RenderEngine.text.x = 105;
RenderEngine.text.y = 125;

RenderEngine.loadCanvas = function () {
    // Control and prep canvas element
    RenderEngine.canvasElement = document.getElementById('gameFrame');
    RenderEngine.canvasElement.setAttribute("width", RLEngine.width+"px");
    RenderEngine.canvasElement.setAttribute("height", RLEngine.height+"px");
    RenderEngine.canvasElement.setAttribute("max-width", RLEngine.width+"px");
    RenderEngine.canvasElement.setAttribute("max-height", RLEngine.height+"px");
    RenderEngine.canvasElement.style.border = "1px solid black";

    // Generate context for engine to draw
    RenderEngine.context = RenderEngine.canvasElement.getContext('2d');

    // Bind onclick for canvas to function determining clicked element
    RenderEngine.canvasElement.onclick = RLEngine.canvasClickHandler;
    console.log("RenderEngine: CanvasElement and Context bound.");
}

RenderEngine.drawBackground = function () {
    RenderEngine.context.beginPath();
    RenderEngine.context.drawImage(RenderEngine.background,0,0);
    RenderEngine.context.stroke();
}

RenderEngine.drawTiles = function (tilesArray) {
    for(var x=0; x<16; x++) {
        for(var y=0; y<16; y++) {
            RenderEngine.context.beginPath();
            RenderEngine.context.drawImage(tilesArray[x][y].img,x*50,y*50);
            RenderEngine.context.stroke();
        }
    }   
}

RenderEngine.drawEntities = function (entitiesArray) {
    for(var i=0; i<entitiesArray.length; i++) {
        RenderEngine.context.beginPath();
        RenderEngine.context.drawImage(
            entitiesArray[i].getImg(),
            entitiesArray[i].x_pos*50,
            entitiesArray[i].y_pos*50);
        RenderEngine.context.stroke();
    }
}

RenderEngine.drawText = function(textQueue) {
    RenderEngine.context.font = RenderEngine.text.font;
    RenderEngine.context.fillStyle = RenderEngine.text.fillStyle;

    // Drawable area (in monospaced font) is 40 wide, 23 tall
    var rowCount = 0;
    var columnCount = 0;
    var rows = ['','','','','','','','','','','','','','','','','','','','','','',''];
    var tempBuffer = RLEngine.textBuffer.slice();
    if(RLEngine.currentScreen != null) {
        tempBuffer.push("█");
        for(var i = 0; i < tempBuffer.length; i++) {
            if(columnCount > 40 || tempBuffer[i] == "newline") {
                if(tempBuffer[i] != "newline") { tempBuffer.splice(i,0,"newline"," "," "); }
                rowCount++;
                columnCount = 0;
            }
            if(rowCount >= 23) {
                rows.shift();
                rows.push('');
                rowCount--;
            }
            if(tempBuffer[i] != "newline"){
                rows[rowCount] += tempBuffer[i];
                columnCount++;
            }
        }
        for(var i = 0; i < rows.length; i++) {
            RenderEngine.context.fillText(rows[i],105,125 + (i*25));
        }
    }
}