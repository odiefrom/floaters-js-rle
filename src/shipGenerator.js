/*
Design for Ship Generation:

Ships have three main designs:

> Block (1):
=======
=======

> Winged (2):
   ======
======
   ======

> Sprawling (3):
==  =  ==
 =======
==  =  ==

At generation start, ship design will be chosen (1/3 odds for each).

Next, design-specific properties are randomly generated.

> Block-ships have determined {LENGTH}X{WIDTH}
> Winged ships can have sub wings off each side of unique lengths, staying symmetrical
> Sprawling ships can have numberous "arms" connected by a central core

Once these properties are determined, the design is broken into rooms.

Rooms are each then randomly generated, storing their results into room tables.

Room tables can be loaded into RLEngine.gamemap on demand.
*/

var ShipGenerator = new Object();

ShipGenerator.padRoomMap = function (originalRoomMap) {
    var boundingLength = originalRoomMap.length;
    var boundingWidth = 0;

    for(var x=0; x<originalRoomMap.length; x++) {
        if(originalRoomMap[x].length > boundingWidth) {
            boundingWidth = originalRoomMap[x].length;
        }
    }

    var lengthOffset = Math.floor((16 - boundingLength) / 2);
    var widthOffset = Math.floor((16 - boundingWidth) / 2);

    originalRoomMap = ShipGenerator.squareUpRoom(originalRoomMap, boundingLength, boundingWidth);

    var newRoomMap = [];

    for(var x=0; x<16; x++) {
        newRoomMap[x] = [];
        for(var y=0; y<16; y++) {
            if(x < lengthOffset || y < widthOffset || x >= (lengthOffset+boundingLength) || y >= (widthOffset+boundingWidth)) {
                newRoomMap[x][y] = Presets.tiles.transparent();
            }
            else {
                newRoomMap[x][y] = originalRoomMap[x-lengthOffset][y-widthOffset];
            }
        }
    }

    return newRoomMap;
}

ShipGenerator.squareUpRoom = function (roomMap, boundingLength, boundingWidth) {
    var outputRoomMap = [];
    for(var x = 0; x < boundingLength; x++) {
        outputRoomMap[x] = [];
        for(var y = 0; y < boundingWidth; y++) {
            if(roomMap[x][y] === undefined) 
            {
                outputRoomMap[x][y] = Presets.tiles.transparent();
            }
            else {
                outputRoomMap[x][y] = roomMap[x][y];
            }
        }
    }
    return outputRoomMap;
}

ShipGenerator.newShip = function () {
    var shipType = ShipGenerator.shipType();
    var ship = null;

    shipType = 1;

    if(shipType == 1) { // BLOCK TYPE
        ship = ShipGenerator.BlockTypeGenerator();
    } else if(shipType == 2) { // WINGED TYPE
        ship = ShipGenerator.WingedTypeGenerator();
    } else if(shipType == 3) { // SPRAWLING TYPE
        ship = ShipGenerator.SprawlingTypeGenerator();
    }
    return ship;
}

ShipGenerator.shipType = function () {
    var rngResult = RNGModule.nextResult();
    return shipNumber = rngResult % 3 + 1;
}

ShipGenerator.BlockTypeGenerator = function() {
    var thisShipSeed = RNGModule.seed;
    var shipLength = Math.abs(RNGModule.nextResult() % 10) + 1;
    var shipWidth = Math.abs(RNGModule.nextResult() % 10) + 1;
    var ship = [];
    ship["rooms"] = [];
    for(var ship_x_pos = 0; ship_x_pos < shipLength; ship_x_pos++) {
        for(var ship_y_pos = 0; ship_y_pos < shipWidth; ship_y_pos++) {
            // Generate room's length/width
            var roomTemp = [];
            var roomLength = Math.abs(RNGModule.nextResult() % 12) + 4;
            var roomWidth = Math.abs(RNGModule.nextResult() % 12) + 4;

            // Fill the room with metal floor tiles
            for(var room_x_pos = 0; room_x_pos < roomLength; room_x_pos++) {
                roomTemp[room_x_pos] = [];
                if(room_x_pos == 0 || room_x_pos == roomLength -1) {
                    continue;
                }
                for(var room_y_pos = 1; room_y_pos < roomWidth-1; room_y_pos++) {
                    roomTemp[room_x_pos][room_y_pos] = Presets.tiles.metalFloor();
                }
            }

            var roomName = ship_x_pos.toString() + ship_y_pos.toString();

            // Provide doors in room as needed
            if(ship_x_pos > 0) { // Need left door
                var tempDoor = Presets.tiles.door();
                tempDoor.currentRoom = roomName;
                tempDoor.doorName = roomName + "left";
                tempDoor.linkedRoom = (ship_x_pos-1).toString() + (ship_y_pos).toString();
                roomTemp[0][Math.round(roomWidth/2)] = tempDoor;
            }
            if(ship_x_pos < shipLength -1 ) { // Need right door
                var tempDoor = Presets.tiles.door();
                tempDoor.currentRoom = roomName;
                tempDoor.doorName = roomName + "right";
                tempDoor.linkedRoom = (ship_x_pos+1).toString() + (ship_y_pos).toString();
                roomTemp[roomLength-1][Math.round(roomWidth/2)] = tempDoor;
            }
            if(ship_y_pos > 0) { // Need up door
                var tempDoor = Presets.tiles.door();
                tempDoor.currentRoom = roomName;
                tempDoor.doorName = roomName + "up";
                tempDoor.linkedRoom = (ship_x_pos).toString() + (ship_y_pos-1).toString();
                roomTemp[Math.round(roomLength/2)][0] = tempDoor;
            }
            if(ship_y_pos < shipWidth - 1) { // Need down door
                var tempDoor = Presets.tiles.door();
                tempDoor.currentRoom = roomName;
                tempDoor.doorName = roomName + "down";
                tempDoor.linkedRoom = (ship_x_pos).toString() + (ship_y_pos+1).toString();
                roomTemp[Math.round(roomLength/2)][roomWidth-1] = tempDoor;
            }

            ship.rooms[roomName] = roomTemp;
        }
    }

    // Determine entryRoom
    var entryRoomY = Math.abs(RNGModule.nextResult()) % shipWidth;
    var entryRoomName = "0" + entryRoomY.toString();

    // Make new door with properties for entryRoom
    var tempEntryDoor = Presets.tiles.door();
    tempEntryDoor.linkedRoom = "playerShip";
    tempEntryDoor.currentRoom = entryRoomName;
    tempEntryDoor.doorName = entryRoomName + "left";

    var entryDoorY = Math.round(ship.rooms[entryRoomName].length / 2);
    ship.rooms[entryRoomName][0][entryDoorY] = tempEntryDoor;

    // Assign playerShip new door properties
    ShipGenerator.updatePlayerShipDoor(entryRoomName);

    RLEngine.gameMap = RLEngine.playerShip;
    RLEngine.foreignShip = ship;
}

// The Player's Ship is a one room block ship type with height/width = 1
// The Player's Ship serves as a constant that connects the experience between other ships.
ShipGenerator.playerShip = function () {
    var ship = new Object();
    ship.entryRoom = [];
    ship.rooms = [];
    for(var x=0; x<7; x++) {
        ship.entryRoom[x] = [];
        for(var y=0; y<7; y++) {
            ship.entryRoom[x][y] = Presets.tiles.metalFloor();
        }
    }
    ship.entryRoom[7] = [];
    ship.entryRoom[7][3] = Presets.tiles.door();
    ship.entryRoom[4][0] = Presets.tiles.terminal();
    ship.entryRoom[7][3].linkedRoom = "00";
    ship.entryRoom[7][3].currentRoom = "playerShip";
    ship.entryRoom[7][3].doorName = "playerShipright";

    ship.rooms["player_ship"] = ship.entryRoom;

    return ship;
}

ShipGenerator.updatePlayerShipDoor = function(newLinkedRoomName) {
    RLEngine.playerShip[11][7].linkedRoom = newLinkedRoomName;
}