var defaults = new Object();

defaults.getJSON = function(){
    return {"files":[
        ["downArrow","downArrow.png"],
        ["upArrow","upArrow.png"],
        ["leftArrow","leftArrow.png"],
        ["rightArrow","rightArrow.png"],
        ["icon","icon.png"],
        ["transparent","transparent.png"],
        ["playerLeft","playerLeft.png"],
        ["playerRight","playerRight.png"],
        ["playerUp","playerUp.png"],
        ["playerDown","playerDown.png"],
        ["doorOpen","doorOpen.png"],
        ["doorClosed","doorClosed.png"],
        ["terminal","terminalToken.png"]
    ]}
}