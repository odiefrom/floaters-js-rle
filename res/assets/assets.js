var assets = new Object();

assets.getJSON = function(){
    return {"files":[
        ["metalFloorTile","metalFloorTile.png"],
        ["spaceTile","spaceTile.png"],
        ["verticalShipWall","verticalShipWall.png"],
        ["spaceBackground","spaceBackground.png"],
        ["terminalScreen","terminalScreen.png"]
    ]};
}